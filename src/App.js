import { useState } from "react";
import Registration from "./components/Registration";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Registration></Registration>
    </div>
  );
}

export default App;
